# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.0] 2024-06-10

### Added
- A lot of fixes and refactoring

### Changed
- pin -> pincode (uci)

## [1.0.0] 2024-02-08

### Added
- Created LTE module

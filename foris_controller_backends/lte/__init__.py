# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import logging
import turrishw
import json

from typing import Optional
from foris_controller_backends.cmdline import BaseCmdLine
from foris_controller_backends.uci import UciBackend, get_option_named
from foris_controller_modules.lte.datatypes import PINErrors

logger = logging.getLogger(__name__)


class LTEUci:
    _INTERFACE = "gsm"

    def __init__(self) -> None:
        self.lte_cmds = LTECmds()

    def get_settings(self) -> dict[str, str]:
        qmi_devices = []

        with UciBackend() as backend:
            data = backend.read("network")

            # get the path of gsm device to pair with sys_path source
            slot_path = get_option_named(data, "network", "gsm", "device", default="")

            # uci records exists, success
            if slot_path:
                # sys_path exists
                hw_device = LTEUci._filter_device_by_path(slot_path)

                if hw_device:
                    payload = {}
                    auth = {}

                    apn = get_option_named(data, "network", LTEUci._INTERFACE, "apn", "internet")
                    pin = get_option_named(data, "network", LTEUci._INTERFACE, "pincode", default="unset")
                    auth_type = get_option_named(data, "network", LTEUci._INTERFACE, "auth", default="none")

                    auth.update({"type": auth_type})

                    if auth_type != "none":
                        username = get_option_named(data, "network", LTEUci._INTERFACE, "username")
                        password = get_option_named(data, "network", LTEUci._INTERFACE, "password")

                        auth.update({"username": username, "password": password})

                    current_status = self.lte_cmds._get_current_modem_status(slot_path)

                    payload.update(
                        {
                            "id": hw_device["id"],
                            "apn": apn,
                            "auth": auth,
                            "qmi_device": hw_device["qmi_device"],
                            "pin": pin,
                            "info": current_status,
                        }
                    )

                    qmi_devices.append(payload)

        return qmi_devices

    def update_settings(self, id, auth, apn, qmi_device, pin=None) -> tuple[bool, Optional[str]]:
        """Update settings for SIM and Modem
        params:
            network: see schema/lte.json"""

        network_needs_restart = False

        thw = turrishw.get_ifaces(filter_types=["wwan"])
        hw_device = thw.get(id, None)

        if hw_device:
            with UciBackend() as backend:
                data = backend.read("network")
                slot_path = get_option_named(data, "network", LTEUci._INTERFACE, "device", default="")

                # check the device is really our device
                if hw_device["qmi_device"] == qmi_device:
                    current_status = self.lte_cmds._get_current_modem_status(slot_path)

                    # Changing PIN

                    # frontend should not send new PIN when SIM not pin-locked
                    if not current_status["sim_lock"] and pin:
                        return False, PINErrors.NOT_LOCKED

                    # uci has not pin yet, card is sim-locked
                    # user try to send the correct PIN that
                    # they have got from operator/provider or pin
                    # they changed in their handheld device
                    if (
                        pin is not None
                        and current_status["sim_lock"]
                        and current_status["state"] == "locked"
                    ):
                        # test if the new pin can be sent to SIM
                        if self.lte_cmds.send_pin(pin):
                            backend.set_option("network", LTEUci._INTERFACE, "pincode", pin)
                            network_needs_restart = True
                        else:
                            return False, PINErrors.INCORRECT_PIN

                    backend.set_option("network", LTEUci._INTERFACE, "apn", apn)

                    # set authorization method, if apn is not enough
                    if auth["type"] in {"pap", "chap", "both"}:
                        backend.set_option("network", LTEUci._INTERFACE, "auth", auth["type"])
                        backend.set_option("network", LTEUci._INTERFACE, "username", auth["username"])
                        backend.set_option("network", LTEUci._INTERFACE, "password", auth["password"])

                    # devices["auth"] == "none"
                    else:
                        backend.del_option("network", LTEUci._INTERFACE, "auth", fail_on_error=False)
                        backend.del_option("network", LTEUci._INTERFACE, "username", fail_on_error=False)
                        backend.del_option("network", LTEUci._INTERFACE, "password", fail_on_error=False)

            # if the PIN has not been set, card is locked
            # we let OpenWRT to attempt PIN
            # using `restart network` action (ifup, ifdown)
            # and this has to be done right after `uci commit`
            if network_needs_restart:
                self.lte_cmds.restart_network()

            return True, None
        return False, None

    @staticmethod
    def _filter_device_by_path(slot_path: str) -> list:
        wwan_interfaces = turrishw.get_ifaces(filter_types=["wwan"])

        for interface, data in wwan_interfaces.items():
            if data["slot_path"] == slot_path:
                return {**data, "id": interface}
        return None


class LTECmds(BaseCmdLine):
    """Only PIN can be changed on SIM, does not let user set sim-PIN-lock.
    This class only fetches runtime data from modem using mmcli"""

    MM = "/usr/bin/mmcli"

    def __init__(self) -> None:
        super().__init__()
        """Save modem state to class, refresh if any chages are expected."""
        self.sys_path = None  # save `sys_path` and use in future calls as default
        self.primary_sim_path = None  # same with `primary_sim_path` as above
        self.sim_lock = False

    def _get_current_modem_status(self, sys_path: str = None) -> dict:
        """Get modem status from `mmcli` command in json
        and return python dictionary with filtered results"""

        retval, res, sterr = self._run_command("/usr/bin/mmcli", "-J", "-m", self.sys_path or sys_path)

        if res:
            try:
                data = json.loads(res)
            except Exception as e:
                logger.error("Cannot fetch modem details.")
                raise e

        # TODO: JSON or Pydantic validation

        # get and return only required items
        state = data["modem"]["generic"]["state"]
        manufacturer = data["modem"]["generic"]["manufacturer"]
        model = data["modem"]["generic"]["model"]
        primary_sim_path = data["modem"]["generic"]["sim"]
        operator = data["modem"]["3gpp"]["operator-name"]
        sim_lock = "sim" in data["modem"]["3gpp"]["enabled-locks"]
        registration = data["modem"]["3gpp"]["registration-state"]  # roaming

        if operator == "--":
            retval, res, sterr = self._run_command(
                "/usr/bin/mmcli", "-J", "-m", self.sys_path or sys_path, "-i", primary_sim_path
            )
            sim_data = json.loads(res)

            operator = sim_data["sim"]["properties"]["operator-name"]

        self.sys_path = sys_path
        self.primary_sim_path = primary_sim_path
        self.sim_lock = sim_lock

        return dict(
            state=state,
            manufacturer=manufacturer,
            model=model,
            operator=operator,
            registration=registration,
            sim_lock=sim_lock,
        )

    def send_pin(self, pin: str):
        """Try if pin is correct"""
        retval, res, sterr = self._run_command(
            LTECmds.MM, "-m", self.sys_path, "-i", self.primary_sim_path, "--pin", pin
        )
        return retval == 0

    def restart_network(self):
        retval, _, _ = self._run_command("/sbin/ifdown", "gsm")
        retval2, _, _ = self._run_command("/sbin/ifup", "gsm")
        return (retval == 0) and (retval2 == 0)

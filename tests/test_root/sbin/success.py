#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023, CZ.NIC z.s.p.o. (https://www.nic.cz/)
import sys


if __name__ == "__main__":
    sys.exit(0)

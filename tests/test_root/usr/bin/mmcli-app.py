#!/usr/bin/env python3

# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2023, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import sys
import json
from pathlib import Path
from argparse import ArgumentParser

FILE_ROOT_PATH = "/tmp/foris_files"
BASE_MMCLI_FILE = Path(FILE_ROOT_PATH) / "tmp/mmcli/modem_data.json"
SIM_MMCLI_FILE = Path(FILE_ROOT_PATH) / "tmp/mmcli/sim_data.json"
SIM_PIN = Path(FILE_ROOT_PATH) / "tmp/mmcli/sim.pin"

WRONG_PIN_MSG = (
    "error: couldn't send PIN code to the SIM: "
    "'GDBus.Error:org.freedesktop.ModemManager1.Error.MobileEquipment.IncorrectPassword: "
    "Couldn't verify PIN: QMI protocol error (12): 'IncorrectPin''"
)


def exit_error(msg: str, retval: int):
    with open(sys.stderr, "w") as o:
        print(msg, file=o)
    sys.exit(retval)


def parse_args():
    parser = ArgumentParser(prog="mmcli")
    parser.add_argument("-J", "--json", action="store_true", default=False)
    parser.add_argument("-m", "--modem")
    parser.add_argument("-i", "--sim")
    parser.add_argument("--pin")
    parser.add_argument("--change-pin")

    return parser.parse_args()


def main():
    args = parse_args()

    file_ = BASE_MMCLI_FILE
    if args.sim is not None:
        file_ = SIM_MMCLI_FILE

    if args.pin:
        with open(SIM_PIN, "r") as rf:
            pin = rf.read()

        if args.pin == pin:
            sys.exit(0)
        else:
            exit_error(WRONG_PIN_MSG, 1)

    if Path(file_).is_file():
        with open(file_, "rb") as f:
            mock_data = json.load(f)

            print(json.dumps(mock_data, indent=2))
    sys.exit(0)


if __name__ == "__main__":
    main()

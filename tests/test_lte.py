# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import pytest
import json
from foris_controller_testtools.fixtures import (
    FILE_ROOT_PATH,
    UCI_CONFIG_DIR_PATH,
    backend,
    device,
    infrastructure,
    network_restart_command,
    only_backends,
    turris_os_version,
    uci_configs_init,
)
from foris_controller_testtools.utils import (
    FileFaker,
    get_uci_module,
    prepare_turrishw,
    prepare_turrishw_root,
)

from copy import deepcopy
from .conftest import ModemType

MMCLI_MOCK_DATA_FILES = "/tmp/mmcli_mock_data.json"
NOTIFICATION_FILTERS = [("lte", "update_settings")]


def get_settings(infrastructure):
    """Helper function not to repeat ourselves.
    :warning: In case additional devices are tested, be sure
    to modify whole tests as for now it takes only one first device
    """

    res = infrastructure.process_message({"module": "lte", "action": "get_settings", "kind": "request"})

    assert "errors" not in res.keys()
    assert "devices" in res["data"]
    assert len(res["data"]["devices"]) >= 1

    first_device = res["data"]["devices"][0]

    assert first_device.keys() == {"id", "qmi_device", "apn", "pin", "auth", "info"}

    return first_device


def update(infrastructure, payload, check_result=True):
    """Send update settings and assert success"""

    res = infrastructure.process_message(
        {"module": "lte", "action": "update_settings", "kind": "request", "data": {"devices": [payload]}}
    )

    if check_result:
        assert "errors" not in res.keys()
        assert res["data"]["result"]

        return True
    else:
        return res


def get_basic_settings(infrastructure):
    """For further editation, delete unneccessary keys.
    To send an setting, you never use old `pin`
    and you don't send `info` either"""
    basic_settings = get_settings(infrastructure)
    basic_settings.pop("info")
    basic_settings.pop("pin")

    return basic_settings


@pytest.mark.parametrize(
    "device,turris_os_version,modem_config", [("omnia", "7.0", (ModemType.EP06, None))], indirect=True
)
def test_notification(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    if infrastructure.backend_name in ["openwrt"]:
        prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    # basic auth

    settings = get_basic_settings(infrastructure)

    auth = {"type": "chap", "username": "noname", "password": "obfuscated"}

    notifications = infrastructure.get_notifications(filters=NOTIFICATION_FILTERS)

    settings["auth"] = auth
    update(infrastructure, settings)

    notifications = infrastructure.get_notifications(notifications, filters=NOTIFICATION_FILTERS)

    assert notifications[-1] == {
        "module": "lte",
        "action": "update_settings",
        "kind": "notification",
        "data": {
            "devices": [
                {
                    "id": "wwan0",
                    "apn": "internet",
                    "auth": {"type": "chap", "username": "noname", "password": "obfuscated"},
                    "qmi_device": "/dev/cdc-wdm0",
                }
            ],
            "result": True,
        },
    }


@pytest.mark.parametrize("modem_config", [(ModemType.EP06, None), (ModemType.EC20, None)], indirect=True)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
def test_get_settings(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    """Basic test to test get_settings function both on openwrt and mock"""
    if infrastructure.backend_name in ["openwrt"]:
        prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    # trigger basic message
    get_settings(infrastructure)


@pytest.mark.parametrize("modem_config", [(ModemType.EP06, None)], indirect=True)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_get_settings_openwrt_ep06(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    """Basic test to get settings on openwrt"""
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    uci = get_uci_module(infrastructure.name)

    # basic setup
    res = get_settings(infrastructure)

    assert res["auth"]["type"] == "none"
    assert res["apn"] == "internet"
    assert res["id"] == "wwan0"
    assert res["qmi_device"] == "/dev/cdc-wdm0"
    assert res["pin"] == "unset"

    # basic setup info
    info = res["info"]

    assert info.keys() == {"state", "manufacturer", "model", "operator", "registration", "sim_lock"}

    assert info["state"] == "connected"
    assert info["manufacturer"] == "Quectel"
    assert info["model"] == "EP06-E"
    assert info["operator"] == "O2.CZ"
    assert info["registration"] == "home"
    assert not info["sim_lock"]

    # test get_settings basic auth and pin
    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        backend.set_option("network", "gsm", "auth", "pap")
        backend.set_option("network", "gsm", "username", "anonymous")
        backend.set_option("network", "gsm", "password", "secret")
        backend.set_option("network", "gsm", "pincode", "8383")

    res = get_settings(infrastructure)
    auth = res["auth"]

    assert auth.keys() == {"type", "username", "password"}

    assert res["pin"] == "8383"
    assert auth["type"] == "pap"
    assert auth["username"] == "anonymous"
    assert auth["password"] == "secret"


@pytest.mark.parametrize("modem_config", [(ModemType.EC20, None)], indirect=True)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_get_settings_openwrt_ec20(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    """Basic test to get settings on openwrt"""
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    uci = get_uci_module(infrastructure.name)

    # basic setup
    res = get_settings(infrastructure)

    assert res["auth"]["type"] == "none"
    assert res["apn"] == "internet"
    assert res["id"] == "wwan0"
    assert res["qmi_device"] == "/dev/cdc-wdm0"
    assert res["pin"] == "unset"

    # basic setup info
    info = res["info"]

    assert info.keys() == {"state", "manufacturer", "model", "operator", "registration", "sim_lock"}

    assert info["state"] == "connected"
    assert info["manufacturer"] == "Quectel"
    assert info["model"] == "EC20"
    assert info["operator"] == "O2 - CZ"
    assert info["registration"] == "home"
    assert not info["sim_lock"]

    # test get_settings basic auth and pin
    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        backend.set_option("network", "gsm", "auth", "pap")
        backend.set_option("network", "gsm", "username", "anonymous")
        backend.set_option("network", "gsm", "password", "secret")
        backend.set_option("network", "gsm", "pincode", "8383")

    res = get_settings(infrastructure)
    auth = res["auth"]

    assert auth.keys() == {"type", "username", "password"}

    assert res["pin"] == "8383"
    assert auth["type"] == "pap"
    assert auth["username"] == "anonymous"
    assert auth["password"] == "secret"


@pytest.mark.parametrize(
    "modem_config",
    [
        (
            ModemType.EP06,
            {"modem": {"3gpp": {"enabled-locks": ["fixed-dialing", "sim"]}, "generic": {"state": "locked"}}},
        )
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_get_settings_another_info(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    res = get_settings(infrastructure)
    info = res["info"]

    assert info["sim_lock"]
    assert info["state"] == "locked"


@pytest.mark.parametrize(
    "modem_config",
    [(ModemType.EP06, {"modem": {"generic": {"state": "disabled"}}})],
    indirect=True,
)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_interface_disabled(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    res = get_settings(infrastructure)
    info = res["info"]
    assert info["state"] == "disabled"


@pytest.mark.parametrize("modem_config", [(ModemType.EP06, None)], indirect=True)
@pytest.mark.parametrize("device,turris_os_version", [("omnia", "7.0")], indirect=True)
def test_update_settings(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    if infrastructure.backend_name in ["openwrt"]:
        prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    # basic auth

    settings = get_basic_settings(infrastructure)

    auth = {"type": "pap", "username": "anonymous", "password": "secret"}

    settings["auth"] = auth
    update(infrastructure, settings)

    # change apn only
    settings = get_basic_settings(infrastructure)
    settings["apn"] = "intranet"
    update(infrastructure, settings)

    # fail change pin (beacuase info.sim_lock=False)
    if infrastructure.backend_name in ["mock"]:
        settings = get_basic_settings(infrastructure)
        settings["pin"] = "1234"
        res = update(infrastructure, settings, False)

        assert not res["data"]["result"]


@pytest.mark.parametrize("modem_config", [(ModemType.EP06, None)], indirect=True)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_update_settings_openwrt(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    if infrastructure.backend_name in ["openwrt"]:
        prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    # test modify auth

    settings = get_basic_settings(infrastructure)

    auth = {"type": "pap", "username": "anonymous", "password": "secret"}

    notifications = infrastructure.get_notifications(filters=NOTIFICATION_FILTERS)

    settings["auth"] = auth
    update(infrastructure, settings)
    res = get_settings(infrastructure)

    auth = res["auth"]

    assert auth["type"] == "pap"
    assert auth["username"] == "anonymous"
    assert auth["password"] == "secret"

    notifications = infrastructure.get_notifications(filters=NOTIFICATION_FILTERS)

    assert notifications[-1] == {
        "module": "lte",
        "action": "update_settings",
        "kind": "notification",
        "data": {
            "devices": [
                {
                    "id": "wwan0",
                    "apn": "internet",
                    "auth": {"type": "pap", "username": "anonymous", "password": "secret"},
                    "qmi_device": "/dev/cdc-wdm0",
                }
            ],
            "result": True,
        },
    }

    uci = get_uci_module(infrastructure.name)

    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        data = backend.read()

        assert uci.get_option_named(data, "network", "gsm", "auth", "") == "pap"
        assert uci.get_option_named(data, "network", "gsm", "username", "") == "anonymous"
        assert uci.get_option_named(data, "network", "gsm", "password", "") == "secret"


@pytest.mark.parametrize("sim_pin", ["8383"], indirect=True)
@pytest.mark.parametrize(
    "modem_config",
    [
        (
            ModemType.EP06,
            {"modem": {"3gpp": {"enabled-locks": ["fixed-dialing", "sim"]}, "generic": {"state": "locked"}}},
        )
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_change_pin_locked(infrastructure, device, turris_os_version, prepare_uci, modem_config, sim_pin):
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    uci = get_uci_module(infrastructure.name)

    # there is no pin
    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        data = backend.read()

        assert uci.get_option_named(data, "network", "gsm", "pincode", "none") == "none"

    settings = get_settings(infrastructure)

    # password can be changed only if sim is PIN locked
    assert settings["info"]["sim_lock"]

    new_settings = get_basic_settings(infrastructure)
    new_settings["pin"] = "8383"

    notifications = infrastructure.get_notifications(filters=NOTIFICATION_FILTERS)
    update(infrastructure, new_settings)
    notifications = infrastructure.get_notifications(notifications, filters=NOTIFICATION_FILTERS)

    assert notifications[-1]["data"]["result"]

    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        data = backend.read()

        assert uci.get_option_named(data, "network", "gsm", "pincode", "none") == "8383"


@pytest.mark.parametrize("modem_config", [(ModemType.EP06, None)], indirect=True)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_pass_update_fail_no_sim_lock(infrastructure, device, turris_os_version, prepare_uci, modem_config):
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    uci = get_uci_module(infrastructure.name)

    # there is no pin
    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        data = backend.read()

        assert uci.get_option_named(data, "network", "gsm", "pincode", "none") == "none"

    settings = get_settings(infrastructure)

    # password cannot be changed
    assert not settings["info"]["sim_lock"]

    new_settings = get_basic_settings(infrastructure)
    new_settings["pin"] = "8383"

    notifications = infrastructure.get_notifications(filters=NOTIFICATION_FILTERS)
    res = update(infrastructure, new_settings, False)
    notifications = infrastructure.get_notifications(notifications, filters=NOTIFICATION_FILTERS)

    assert not notifications[-1]["data"]["result"]

    assert not res["data"]["result"]
    assert "error_code" in res["data"].keys()
    assert res["data"]["error_code"] == "not-locked"

    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        data = backend.read()

        assert uci.get_option_named(data, "network", "gsm", "pincode", "none") == "none"


@pytest.mark.parametrize("sim_pin", ["0000"], indirect=True)
@pytest.mark.parametrize(
    "modem_config",
    [
        (
            ModemType.EP06,
            {"modem": {"3gpp": {"enabled-locks": ["fixed-dialing", "sim"]}, "generic": {"state": "locked"}}},
        )
    ],
    indirect=True,
)
@pytest.mark.parametrize(
    "device,turris_os_version", [("omnia", "7.0"), ("mox", "7.0"), ("turris", "7.0")], indirect=True
)
@pytest.mark.only_backends(["openwrt"])
def test_update_wrong_pin(infrastructure, device, turris_os_version, prepare_uci, modem_config, sim_pin):
    """Card is locked and user sends wrong pin."""
    prepare_turrishw(f"{device}-wwan-{turris_os_version}")

    new_settings = get_basic_settings(infrastructure)
    new_settings["pin"] = "1234"

    res = update(infrastructure, new_settings, False)

    assert not res["data"]["result"]
    assert "error_code" in res["data"].keys()
    assert res["data"]["error_code"] == "incorrect-pin"

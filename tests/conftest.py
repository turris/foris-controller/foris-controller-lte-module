# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

from pathlib import Path
from enum import Enum
from typing import Any

import pytest
import json

from foris_controller_testtools.fixtures import UCI_CONFIG_DIR_PATH, FILE_ROOT_PATH
from foris_controller_testtools.utils import get_uci_module, FileFaker


PRIMARY_DEVICE_PATH = {
    "omnia": "/sys/devices/platform/soc/soc:internal-regs/f1058000.usb/usb1/1-1",
    "mox": "/sys/devices/platform/soc/soc:internal-regs@d0000000/d005e000.usb/usb1/1-1",
    "turris": "/sys/devices/platform/ffe08000.pcie/pci0002:00/0002:00:00.0/0002:01:00.0/usb2/2-2",
}

MMCLI_MOCK_DATA_FILES = Path("/tmp/mmcli/")
TEST_DIR = Path(__file__).parent


class ModemType(Enum):
    EP06 = "ep06"
    EC20 = "ec20"


def deep_update(mapping: dict[str, Any], *updating_mappings: dict[str, Any]) -> dict[str, Any]:
    """Helper function to update inner mappindg non-destructively and keep subkeys untouched"""
    updated_mapping = mapping.copy()
    for updating_mapping in updating_mappings:
        for k, v in updating_mapping.items():
            if isinstance(updated_mapping.get(k), dict) and isinstance(v, dict):
                updated_mapping[k] = deep_update(updated_mapping[k], v)
            else:
                updated_mapping[k] = v
    return updated_mapping


@pytest.fixture(scope="function")
def modem_config(request):
    """Takes default modem config and mocks the files that the
    testing mmcli scripts loads.
    It is possible to provide update to these default settings providing
    a dictionary
    :param: tuple[<model>,<update>]
    :model: ModemType to detemine which base files to use for mock
    :update: concrete update settings, does not overwrite whole subkey"""
    model, update = request.param

    # load base config based on modem
    with open(TEST_DIR / f"base_modem_{model.value}.json", "r") as f:
        data = json.load(f)

    if update is not None:
        data = deep_update(data, update)

    sim_data = json.load((TEST_DIR / "base_sim.json").open("r"))

    with (
        FileFaker(
            FILE_ROOT_PATH, str(MMCLI_MOCK_DATA_FILES / "modem_data.json"), False, json.dumps(data, indent=2)
        ) as base_fake,
        FileFaker(
            FILE_ROOT_PATH, str(MMCLI_MOCK_DATA_FILES / "sim_data.json"), False, json.dumps(sim_data, indent=2)
        ) as sim_fake,
    ):
        yield base_fake, sim_fake


@pytest.fixture(scope="function")
def sim_pin(request):
    """Simulates pin on SIM card"""

    pin = request.param

    with FileFaker(FILE_ROOT_PATH, str(MMCLI_MOCK_DATA_FILES / "sim.pin"), False, pin) as pin_fake:
        yield pin_fake


@pytest.fixture(scope="session")
def uci_config_default_path():
    return Path(__file__).resolve().parent / "uci_configs"


@pytest.fixture(scope="session")
def cmdline_script_root():
    return Path(__file__).resolve().parent / "test_root"


@pytest.fixture(scope="session")
def file_root():
    return Path(__file__).resolve().parent / "test_root"


@pytest.fixture(scope="module")
def controller_modules():
    return ["remote", "lte"]


@pytest.fixture()
def prepare_uci(infrastructure, device):
    uci = get_uci_module(infrastructure.name)

    with uci.UciBackend(UCI_CONFIG_DIR_PATH) as backend:
        backend.set_option("network", "gsm", "device", PRIMARY_DEVICE_PATH.get(device))


def pytest_addoption(parser):
    parser.addoption(
        "--backend",
        action="append",
        default=[],
        help=("Set test backend here. available values = (mock, openwrt)"),
    )
    parser.addoption(
        "--message-bus",
        action="append",
        default=[],
        help=("Set test bus here. available values = (unix-socket, ubus, mqtt)"),
    )
    parser.addoption(
        "--debug-output",
        action="store_true",
        default=False,
        help=("Whether show output of foris-controller cmd"),
    )


def pytest_generate_tests(metafunc):
    if "backend" in metafunc.fixturenames:
        backend = metafunc.config.option.backend
        if not backend:
            backend = ["openwrt"]
        metafunc.parametrize("backend_param", backend, scope="module")

    if "message_bus" in metafunc.fixturenames:
        message_bus = metafunc.config.option.message_bus
        if not message_bus:
            message_bus = ["mqtt"]
        metafunc.parametrize("message_bus_param", message_bus, scope="module")

# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

__version__ = "1.1.0"

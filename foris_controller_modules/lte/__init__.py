# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import copy
import logging

from foris_controller.handler_base import wrap_required_functions
from foris_controller.module_base import BaseModule


class LTEModule(BaseModule):
    logger = logging.getLogger(__name__)

    def action_get_settings(self, data):
        return {"devices": self.handler.get_settings()}

    def action_update_settings(self, data):
        new_settings = copy.deepcopy(data)

        res = {}
        success, msg = self.handler.update_settings(**data["devices"][0])
        self.notify("update_settings", {**new_settings, "result": success})
        res = {"result": success}
        if msg:
            res.update({"error_code": msg})
        return res


@wrap_required_functions(["get_settings", "update_settings"])
class Handler:
    pass

# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import logging

from foris_controller.handler_base import BaseOpenwrtHandler
from foris_controller.utils import logger_wrapper

from foris_controller_backends.lte import LTECmds, LTEUci

from .. import Handler

logger = logging.getLogger(__name__)


class OpenwrtLTEHandler(Handler, BaseOpenwrtHandler):
    cmds = LTECmds()
    uci = LTEUci()

    @logger_wrapper(logger)
    def get_settings(self):
        return OpenwrtLTEHandler.uci.get_settings()

    @logger_wrapper(logger)
    def update_settings(self, id, auth, apn, qmi_device, pin=None):
        return OpenwrtLTEHandler.uci.update_settings(id, auth, apn, qmi_device, pin)

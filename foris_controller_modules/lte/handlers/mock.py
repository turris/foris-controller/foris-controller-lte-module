# foris-controller-lte-module
#
# SPDX-License-Identifier: GPL-3.0-or-later
# Copyright 2024, CZ.NIC z.s.p.o. (https://www.nic.cz/)

import logging

from foris_controller.handler_base import BaseMockHandler
from foris_controller.utils import logger_wrapper
from foris_controller_modules.lte.datatypes import PINErrors

from .. import Handler

logger = logging.getLogger(__name__)


class MockLTEHandler(Handler, BaseMockHandler):
    EXAMPLE_SETTINGS = [
        {
            "id": "wwan0",
            "apn": "internet",
            "auth": {"type": "none"},
            "qmi_device": "/dev/cdc-wdm0",
            "pin": "unset",
            "info": {
                "state": "connected",
                "manufacturer": "Quectel",
                "model": "EP06-E",
                "operator": "P-Gauss",
                "registration": "home",
                "sim_lock": False,
            },
        }
    ]

    def __init__(self):
        super().__init__()
        self._settings = MockLTEHandler.EXAMPLE_SETTINGS

    @logger_wrapper(logger)
    def get_settings(self):
        return self._settings

    @logger_wrapper(logger)
    def update_settings(self, id, auth, apn, qmi_device, pin=None):
        modem = self._filter_device(self._settings, id)
        if not modem:
            return False, None

        modem["apn"] = apn

        auth_type = modem["auth"]["type"]
        modem["auth"] = {"type": auth_type}

        if auth_type != "none":
            modem["auth"].update({"username": auth.get("username", ""), "password": auth.get("password", "")})

        sim_lock = modem.get("sim_lock", False)

        if sim_lock and pin:
            modem["pin"] = pin
        elif pin and not sim_lock:
            return False, PINErrors.NOT_LOCKED

        self._settings.pop()
        self._settings.append(modem)

        return True, None

    @logger_wrapper(logger)
    def list(self):
        return MockLTEHandler.data[: MockLTEHandler.slices]

    @staticmethod
    def _filter_device(payload, id):
        for item in payload:
            if item["id"] == id:
                return item

from enum import Enum


class PINErrors(str, Enum):
    INCORRECT_PIN = "incorrect-pin"
    NOT_LOCKED = "not-locked"
